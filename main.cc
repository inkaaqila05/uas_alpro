/** @author Inka Aqila N
* @NIM 301230043
* @version 08 Februari 2024
* @UAS Algoritma dan Pemrograman
*/

#include <iostream>
#include "myheader.h"

using namespace std;

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;

    tampilkanHasil(absen, tugas, quiz, uts, uas);

    nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;

    Huruf_Mutu = hitungHurufMutu(nilai);

    cout << "Huruf Mutu : " << Huruf_Mutu) << endl;

    return 0;
}
