#include <iostream>
#include "myheader.h"

using namespace std;

char hitungHurufMutu(double nilai) {
    char Huruf_Mutu;

    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    return Huruf_Mutu;
}

void tampilkanHasil(double absen, double tugas, double quiz, double uts, double uas) {
    cout << "Absen = " << absen << endl;
    cout << "Tugas = " << tugas << endl;
    cout << "Quiz = " << quiz << endl;
    cout << "UTS = " << uts << endl;
    cout << "UAS = " << uas << endl;
}
